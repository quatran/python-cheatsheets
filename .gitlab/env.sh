#!/bin/bash
#
# See here for more details:
# https://gitlab.com/help/ci/variables/README.md

ENVS="CI_COMMIT_TAG
CI_COMMIT_REF_NAME
CI_COMMIT_TITLE
CI_COMMIT_SHA
CI_ENVIRONMENT_SLUG
CI_JOB_ID
CI_JOB_URL
CI_RUNNER_ID
CI_RUNNER_TAGS
CI_PIPELINE_ID
CI_PROJECT_NAMESPACE
CI_PROJECT_PATH_SLUG
CI_SERVER_NAME
GITLAB_CI
GITLAB_FEATURES
"

echo "*** GitLab Environment ***"

for i in $ENVS; do
  echo "$i=\"$(eval echo \$$i)\""
done
