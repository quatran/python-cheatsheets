# Cheatsheets for Python #

This repository contains LaTeX files to create cheatsheets for different
topics of the Python programming language. Currently the following
stylesheets exist:

* `dict-cheatsheet.tex`: dictionary cheatsheet
  Covers how to create them, to iterate over, to unpack, to merge, available
  operations, and more.

* `oop1-cheatsheet.tex`: object oriented programming, part 1, basics
  Covers basic terminology, four pillars of OOP, properties of a Python class,
  examples, create classes, specific objects, naming conventions, class and
  instance variables, methods, magic methods, and more.

* `oop2-cheatsheet.tex`: object oriented programming, part 2, relationships
  Covers terminology, SOLID-OOP principles, basic rules for creating classes,
  association, composition, inheritance, duck typing, polymorphy,

* `pytest-cheatsheet.tex`: writing tests with pytest and assert
  Covers terminology, types of tests, recommendations, procedure for TDD,
  describes pytest, how to configure it, how to execute tests, mark tests,
  parametrize tests, using fixtures.

* `python-cheatsheet.tex`: basic Python introduction
  Covers a name of Python executable, how to get help, reserved key words,
  builtin datatypes, assignments, control structures, functions, sequences,
  generators, comprehensions, exception handling, and more.

* `pythonprojekt-cheatsheet.tex`: packaging Python projects
  Covers terminology, creating a directory structure, `setup.py`, creating
  scripts with `setup.py`, virtual Python environment, documenting your
  project, testing your project, uploading to PyPI, and more.

* `seq-cheatsheet.tex`: sequences, lists, tuples, and ranges
  Covers definition, how to create lists, tuples, and range objects, common
  operations, iterate over sequences, slices, sorting, and more.

* `string-cheatsheet.tex`: Strings
  Covers definition, how to create strings, escape sequences, string methods,
  formatting strings, and more.


Currently only in German.

All PDFs are availble from the [download](https://gitlab.com/tomschr/python-cheatsheets/-/jobs/artifacts/master/download?job=Spickzettel) icon.

## Requirements ##

For openSUSE, you need the following packages:

* texlive-babel-german
* texlive-babel-english
* texlive-enumitem
* texlive-fancyvrb
* texlive-koma-script
* texlive-latex-bin-bin
* texlive-listings
* texlive-pgf-umlcd
* texlive-ucs

Use this commandline to install it on openSUSE:

```
$ sudo zypper in --no-recommends texlive-babel-german texlive-babel-english \
  texlive-enumitem \
  texlive-fancyvrb texlive-koma-script texlive-latex-bin-bin texlive-listings \
  texlive-pgf-umlcd texlive-ucs
```


## Building ##

To build the LaTeX files, use one of the following methods:

* Build all files with the `./build.sh` script.

* Run `pdflatex` in the root directory of your local checkout:

      $ export TEXINPUTS="$PWD/sty:"
      $ pdflatex -output-directory=de/pdf/ de/TEX_FILE
