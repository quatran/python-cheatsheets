\documentclass[a4paper,landscape,10pt,ngerman]{scrartcl}
\usepackage{cheatsheet}

%opening
\title{Python Spickzettel}
\subtitle{Sequenzen (Strings, Listen, Tupels)}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}
\raggedcolumns

\begin{document}
\selectlanguage{ngerman}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
Eine Sequenz ist eine Folge mehrerer Objekte. Die wichtigsten sind:
   
\begin{itemize}
\item Strings (Zeichenketten)
\item Listen
\item Tupel
\end{itemize}

Siehe auch \url{http://docs.python.org/2.7/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange}.

\section{Gemeinsame Operationen}

\begin{itemize}
\item \pyth{x in seq}: ist Element x in Sequenz seq enthalten?
\item \pyth{x not in seq}: ist Element x in Sequenz seq \emph{nicht} enthalten?
\item \pyth{seq + t}: Zusammenfügen der Sequenzen seq und t
\item \pyth{seq * n} oder \pyth{n * seq}: n-mal Kopien der Sequenz seq erzeugen
\item \pyth{seq[i]}: i-tes Element der Sequenz seq (Indizierung); die Zählweise beginnt bei Null, negative Indizes sind
erlaubt und zählen vom Ende
\item \pyth{seq[i:j]}: Ausschnitt aus Sequenz seq erzeugen, angefangen vom i-tem bis j-tem Element 
(nicht einschließlich)
\item \pyth{seq[i:j:k]}: Ausschnitt aus Sequenz seq erzeugen wie bei \pyth{seq[i:j]}, jedoch mit Schrittweite k
\item \pyth{len(seq)}: Länge der Sequenz seq, d.~h. Anzahl der Elemente innerhalb von seq
\item \pyth{min(seq)}: kleinstes Element der Sequenz seq
\item \pyth{max(seq)}: größtes Element der Sequenz seq
\end{itemize}


\section{Strings (Zeichenketten)}
Ein String (=Zeichenkette) ist eine unverändliche Sequenz aus Zeichen.
In Python~2 unterscheidet man:
   
\begin{itemize}
\item ASCII-Strings, die nur ASCII-Zeichen enthalten dürfen
\item Unicode-Strings, die zusätzlich Unicode-Zeichen enthalten dürfen
\end{itemize}

Python~3 hat die Unterscheidung zwischen ASCII- und Unicode-Strings nicht mehr,
alle Strings sind vom Typ Unicode.

\subsection{Strings definieren}
Definition über dreifache, doppelte oder einfache Anführungszeichen:
\begin{python}
>>> # Drei Strings definieren:
>>> s1='This is a string'
>>> s2="This is another string"
>>> s3="""Ein dreifach gequoteter String \
... wie hier kann auch 'Quotes' und "Quotes" 
... enthalten. Wir koennen sogar \
... Zeilenumbrueche maskieren, so ist \
... dieser String nur zwei Zeilen lang."""
\end{python}

Achtung: Strings die Unicode-Zeichen enthalten werden in Python 2 als 
Unicode-String (\verb!u"..."!) definiert:

\begin{python}
>>> s4=u"Euro: \N{euro sign} oder \u20ac"
\end{python}

Sonderzeichen wie Backslash (\verb!\!), Anführungszeichen, Zeilenumbruch
müssen als sog.\ \iqq{Escape-Sequenz} angegeben werden.
Siehe Tabelle in \url{http://docs.python.org/2.7/reference/lexical_analysis.html#strings}.

\subsection{In einen String umwandeln}
Verwende die Funktion \pyth{str} (ASCII-Strings) oder \pyth{unicode} (Unicode-Strings):

\begin{python}
>>> str(5)
'5'
>>> unicode(5)
u'5'
\end{python}

Siehe auch \url{http://docs.python.org/2/howto/unicode.html}.

\subsection{Länge eines Strings}
Über die Funktion \cmd{len}:
\begin{python}
>>> len(s1)
16
>>> len(s2)
22
\end{python}


\subsection{Strings ausgeben}

\begin{itemize}
\item Interaktiv; 
\item mit \func{print} (in Skripten);
\item mit \func{sys.stdout.write} (in Skripten).
\end{itemize}


\subsubsection{Interaktiv}
Im interaktiven Modus braucht nur die Variable angegeben zu werden:
   
\begin{python}
>>> s1
'This is a string'
\end{python}

Im interaktiven Modus wird die \iqq{Stringrepresentation} erzeugt, d.~h. der
String wird in Hochkommata ausgegeben. Zeilenumbrüche, Tabulatoren und andere
Sonderzeichen werden durch eine Escape-Sequenz ausgedrückt:
   
\begin{python}
>>> tmp="""Hallo
... Welt!"""
>>> tmp
'Hallo\nWelt!'
\end{python}

\subsubsection{Durch print}
Es gibt zwei Syntaxformen zwecks Kompatibilität zu Python~3:

\begin{itemize}
\item \func{print} \verb!parameters! wird nur in Python 2 verwendet;
\item \func{print}\verb!(parameters)! in Python 3 verwendet. \\
Kann auch in Python~2 verwendet werden. Dazu als erste ausführbare Zeile
im Skript folgendes einfügen:
\begin{python}
from __future__ import print_function
\end{python}
\end{itemize}

Mit Hilfe der \func{print}-Funktion kann die Ausgabe des Strings
auf vielfältige Art beeinflusst werden:

\begin{python}
>>> print(s1)
This is a string
>>> print("s1 =", s1)
s1 = This is a string
>>> print("  s1 (mit sep)", s1, sep="=")
  s1 (mit sep)=This is a string
>>> print("  s1", s1, ", s2", s2, sep="=")
  s1=This is a string=, s2=This is another string
\end{python}

Mehr: \url{http://docs.python.org/2.7/library/functions.html#print}.

\subsection{Strings formatieren}
\begin{itemize}
\item mit dem \%-Operator, ähnlich wie die \pyth{sprintf()}-Funktion in C;
\item mit der \pyth{.format()}-Methode (\iqq{format specifications}) eines Formatstrings
\end{itemize}

\subsubsection{\%-Operator}

\begin{python}
>>> "Hallo %s" % "Welt"
'Hallo Welt'
>>> "Hallo %s %s" % ("Tux", "Pinguin")
'Hallo Tux Pinguin'
>>> "0x%X" % 200
'0xC8'
\end{python}

Mehr: \url{http://docs.python.org/2/library/stdtypes.html#string-formatting}


\subsubsection{Format-String}
Syntax: FormatString.format(Objekte)

\begin{python}
>>> '{0}, {1}, {2}'.format('a', 'b', 'c')
'a, b, c'
>>> '{2}, {1}, {0}'.format('a', 'b', 'c')
'c, b, a'
>>> 'Coordinates: {lat}, {long}'.format(
...  lat='37.24N',
...  long='-115.81W')
'Coordinates: 37.24N, -115.81W'
\end{python}

Mehr: \url{http://docs.python.org/2/library/string.html#formatstrings}


\subsection{Indizieren: Auf einzelnes Zeichen zugreifen}
Syntax: seq[INDEX]

Strings beginnen immer bei Null.

\begin{python}
>>> print("  s1='%s'" % s1)
>>> print("      0123456789|123456789")
>>> print("  s1[5]='%s'" % s1[5])
>>> print("  s1[3]='%s'" % s1[3])
\end{python}

erzeugt:

\begin{python}   
  s1='This is a string'
      0123456789|123456789
  s1[5]='i'
  s1[3]='s'
\end{python}

Negative Zahlen sind als Index ebenso erlaubt:

\begin{python}
>>> print("  s1[-5]='%s'" % s1[-5])
s1[-5]='t'
\end{python}

\subsection{Stringmethoden}
Mehr: \url{http://docs.python.org/2/library/stdtypes.html#string-methods}

\subsection{Strings ändern}
Strings sind \emph{unveränderbar}, d.~h.\ sie können nicht direkt geändert werden
(zu Gründen, siehe \url{http://effbot.org/pyfaq/why-are-python-strings-immutable.htm}).
Dieses funktioniert \emph{nicht}:
   
\begin{python}
>>> s="Hello world!"
>>> s[6]='W'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support
item assignment
\end{python}

Der Umweg erfolgt über Listen:

\begin{python}
>>> l=list(s)
>>> L[6]='W'
>>> "".join(L)
'Hello World!'
\end{python}

oder als Einzeiler über Indizierung:

\begin{python}
>>> s[:6] + "W" + s[7:]
'Hello World!'
\end{python}


\subsection{Ausschnitte (Slicing)}
%
Syntax: seq[START], seq[START:END], seq[START:END:STEP]

\begin{python}
>>> s1[:5]
'This '
>>> s1[5:]
'is a string'
>>> s1[2:5]
'is '
>>> s1[::2]
'Tisg'
\end{python}

\subsection{Strings vergleichen}

\begin{python}
>>> s1 == s2
False
>>> s1[:6] == s2[:6]
True
\end{python}


\subsection{Strings auftrennen und zusammensetzen}

\begin{python}
>>> s1.split(' ')
['This', 'is', 'a', 'string']
>>> s1.split(' is ')
['This', 'a string']
>>> s1 + s2
'This is a stringThis is another string'
>>> '--'.join([s1,s2])
'This is a string--This is another string'
>> '--'.join(s1+s2)
# Try it!
\end{python}


\subsection{Strings umwandeln}

Syntax: seq.FUNC(...)\\
Siehe auch \url{http://docs.python.org/2.7/library/stdtypes.html#string-methods}

\begin{python}
>>> s1.upper()
'THIS IS A STRING'
>>> s1.lower()
'this is a string'
>>> s1.title()
'This Is A String'
>>> s1.swapcase()
'tHIS IS A STRING'
\end{python}


\section{Listen und Tupel}

\begin{itemize}
\item Listen: veränderbare, geordnete Sequenz von null oder mehren Elementen
\item Tupel: unverändliche, geordnete Sequenz von null oder mehren Elementen
\end{itemize}

\subsection{Listenmethoden}

\begin{itemize}
\item \pyth{L.append(x)} Objekt x ans Ende der Liste L einfügen
\item \pyth{L.count(x)} Anzahl der Objekte x in Liste L zurückgeben
\item \pyth{L.extend(m)} oder \pyth{L += m} Liste m ans Ende der Liste L anhängen
\item \pyth{L.index(x, start, end)} Indexposition für Objekt x in Liste L zurückgeben
\item \pyth{L.insert(i, x)} Objekt x in Liste L an Indexposition i einfügen
\item \pyth{L.pop()} Ganz rechtes Objekt in der Liste L zurückgeben und entfernen
\item \pyth{L.pop(i)} Objekt an Indexposition i der Liste L zurückgeben und entfernen
\item \pyth{L.remove(x)} Ganz linksstehendes Objekt der Liste L entfernen
\item \pyth{L.reverse()} Liste L umkehren (in-place)
\item \pyth{L.sort(...)} Liste L sortieren
\end{itemize}

\subsection{Tupelmethoden}

\begin{itemize}
\item \pyth{T.count(x)} Anzahl der Objekte x in Tuple T zurückgeben
\item \pyth{T.index(x, start, end)} Indexposition für Objekt x in Tupel T zurückgeben
\end{itemize}

\subsection{Listen/Tupel definieren}
Listen werden in eckigen Klammern, Tupel in runden Klammern definiert:

\begin{python}
>>> L=[1,5,6,10]
>>> L=(5, "Hallo Welt", 10)
\end{python}


\subsection{In eine Liste/Tupel umwandeln}
Syntax: \pyth{list(sequence)}, \pyth{tuple(sequence)}
Funktionen \pyth{list} und \pyth{tuple} wandeln andere Sequenzen 
in eine Liste oder Tupel um:

\begin{python}
>>> list("Hallo")
['H', 'a', 'l', 'l', 'o']
>>> tuple("Hallo")
('H', 'a', 'l', 'l', 'o')
\end{python}


\subsection{Länge einer Liste/Tupel}
Über die Funktion \pyth{len}:
   
\begin{python}
>>> len([1,2,3])
3
>>> len((2,3,4,5))
4
\end{python}

\subsection{Indizieren von Listen/Tupel}

\begin{python}
>>> L=[3,-4,6]
>>> L[2]
6
>>> L[-1]
6
>>> L[10]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
\end{python}


\subsection{Listen ändern}

\begin{python}
>>> L=[3,-4,6,10,5,12]
>>> L[1]=2
>>> L
[3, 2, 6, 10, 5, 12]
\end{python}


\subsection{Listen erweitern}
\begin{python}
>>> L=[3,-4,6,10,5,12]
>>> L.append(50)
>>> L.extend([100])
>>> L
[3, -4, 6, 10, 5, 12, 50, 100]
\end{python}

% -------------------------------------
\end{multicols}
\end{document}
