\documentclass[a4paper,landscape,10pt,ngerman]{scrartcl}
\usepackage{cheatsheet}

%opening
\title{Python3 Spickzettel}
% \titlehead{fooo}
\subtitle{Schreiben von Tests in Python mit \cmd{pytest} und \pyth{assert}}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\selectlanguage{ngerman}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
% multicol parameters
% These lengths are set only within the two main columns
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}
\begin{abstract}
\noindent\gquote{Im Grunde genommen geht es beim Testen darum, Risiken zu
        reduzieren.} --- \url{usersnap.com}\cite{usersnap.com}

\gquote{Program testing can be used to show the presence of bugs, but never
        show their absence!}\\\hfill -- Edsger W. Dijkstra
\end{abstract}

\section{Definition}
Nach Pol, Koomen und Spillner:

\begin{quote}
\gquote{Unter Testen versteht man den Prozess des Planens, der Vorbereitung
        und der Messung, mit dem Ziel, die Eigenschaften eines
        IT-Systems festzustellen und den Unterschied zwischen dem
        tatsächlichen und dem erforderlichen Zustand aufzuzeigen.}
\end{quote}

\section{Wozu testen?}
\label{sec.pytest.why}
Testen prüft und bewertet Software auf ihre Qualität und die Erfüllung
ihrer definierten Anforderungen.

Ein Test kann nicht den Beweis erbringen, dass keine Fehler in der
Software vorhanden sind (siehe obiges Zitat von Dijkstra).
Jedoch ist Testing durchaus nützlich:

% https://towardsdatascience.com/2-reasons-why-developers-avoid-writing-tests-7e55515776e9

\begin{itemize}
\item Ermöglicht volle Freiheit für Änderungen ohne Angst haben zu müssen, dass
  Fehler eingebaut werden.
\item Unterstützt die Fehlersuche.
\item Ermöglicht die Automatisierung von Tests.
\item Hilft bei der Einführung von neuen Entwicklern ins Team.
\item Agiert als eine Form der Wissenvermittlung.
\item Entdeckt \gquote{blinde Flecke} in unserem Code.
\item Erhöht die Vertrauenswürdigkeit einer Software.
% \item Hilft beim Sparen von Geld (nicht kurzfristig, eher langfristig).
\end{itemize}


\section{Terminologie}
\begin{description}
\item[Assertion]
Prüft innerhalb der Testfunktion, ob das erhaltene Ergebnis mit
dem erwarteten Ergebnis übereinstimmt.

\item[Exception]
Außergewöhnliches Ereignis, das
während der Ausführung des Programms auftritt.
Ursache können ungültige Argumente sein, Netzwerkfehler,
fehlende Schreibrecht u.~a.

\item[Fixture]
Funktion, welche einer Testfunktion Daten übergibt.

%\iffalse
\item[Setup- und Teardown-Funktionen]
Einrichten (Setup) bzw.\ Aufräumen (Teardown) um Bedingungen
zu schaffen, damit eine Test-Suite durchführbar wird.
Beispielsweise die Erstellung einer temporären Datenbank oder das
Starten eines Serverprozesses.
Setup- und Teardown-Funktionen können pro Testfunktion, Modul oder
Global definiert werden.
%\fi

\item[Test-Case]
Individuelle Einheit eines Tests.

\item[Test-Suite]
Menge von Tests, um zu zeigen, dass ein Programm
ein bestimmtes Verhalten zeigt.
\end{description}


\section{Typen von Tests}
Tests können anhand der Größe der zu testenden Objekte
unterschieden werden (sortiert von klein nach groß):

\begin{description}
\item[Ebene 1: Unit Testing]
Überprüft individuelle Funktionen, Klassen oder Module, isoliert
vom Rest des Systems.

\item[Ebene 2: Integration Testing]
Überprüft, wenn einzelne Einheiten kombiniert und als Gruppe
gestestet werden, vielleicht mehrere Klasse oder ein Subsystem.
% Beispielsweise, ob verschiedene Module oder Dienste mit deiner
% Anwendung zusammenpassen.
Größer als ein Unit Test, aber kleiner als ein Systemtest.

\item[Ebene 3: System Testing (\gquote{end-to-end})]
Überprüft, ob das gesamte zu testende System
\emph{als Ganzes} den spezifizierten Anforderungen entspricht.
Hierbei wird das System so nah wie möglich an der Umgebung des
Endbenutzers ausgerichtet.

\item[Ebene 4: Acceptance Testing]
Überprüft und bewertet ein System auf seine Eignung für
die tatsächlichen Anforderungen, Bedürfnisse oder Erwartungen
des Kunden.
% Es beurteilt, ob es die Abnahmekriterien erfüllt und
% für eine Auslieferung tauglich ist.
\end{description}

% Die Welt des \gquote{Software-Testing} kann verwirrend sein, weil
% viele unter dem Begriff \emph{Testing} verschiedenes verstehen.

Software-Testing kann jedoch auch anhand der Methode gruppiert werden:

\begin{description}
\item[Black-box Testing]
testet die Software indem auf die Ein- und Ausgaben (das äußerlich
sichtbare Verhalten) fokusiert wird. Die Implementierung ist
unwichtig.
Beispiel: Webseite, die für eine bestimmte Eingabe eine bestimmte
Darstellung erzeugt.

\item[White-box Testing]
testet die Software anhand ihrer internen Struktur, Design
oder Code. Die Implementierung ist wichtig.
% Der Fokus liegt hauptsächlich auf den Code
% bzw.\ die Verarbeitung von Eingabe und Ausgabe.
Beispiel: eine Funktion die verschiedene if-Abfragen enthält.
Der Entwickler muss wissen, welche Eingaben er benötigt,
damit alle Zweige der Abfrage in einer Testfunktion durchlaufen
werden.
\end{description}

\iffalse
Manche dieser Terme werden je nach Autor oder Seite anders definiert.
Zudem gibt es noch eine Reihe weiterer Definitionen die sich auf
andere Aspekte beziehen (Stress Testing, Performance Testing usw).
Für diesen Spickzettel haben sie jedoch keine weitere Bedeutung.
\fi

\section{Empfehlungen zu Tests und Umgebung}
Um Testing sinnvoll zu nutzen, hier ein paar Empfehlungen:

\begin{description}
\item[Schreibe bevorzugt kleine Tests]

Ein Test muss überschaubar sein, d.~h.\ er sollte wenige
Codezeilen enthalten.
Das ist schneller, besser zu verstehen und einfacher zu warten.

\item[Konzentriere dich auf \emph{eine} Test-Funktionalität]

Ein Test hat genau \emph{eine} Aufgabe (single responsibility).
Eine Funktion bekommt eine oder mehrere Test-Funktionen.

\iffalse
Eine Funktionalität ist eine Testfunktion. Wenn
du beispielsweise Addition und Subtraktion testen möchtest,
schreibe zwei separate Testfunktionen.
In Bezug auf OOP hat ein Test genau eine Aufgabe (single responsibility).
\fi
%
% Vermische keine Funktionalitäten, d.~h.\ teste in einer Funktion nicht
% beispielsweise Addition und Subtraktion sondern verwende verschiedene
% Testfunktionen für diese Aufgabe.

\item[Optimiere deine \gquote{Testabdeckung}]

Die \emph{Testabdeckung} (engl. \emph{test coverage}) ist eine Technik
um zu bestimmen, wieviel Zeilen deines Codes tatsächlich durch Tests
ausgeführt wurden. Dadurch lassen sich Stellen im Code identifizieren,
die noch Tests benötigen.

% Er wird berechnet als ${T}/{A}*100$;
% $T$ bedeutet die Anzahl der ausgeführten Zeilen der Testsuite und
% $A$ die Gesamtzahl des zu testenden Codes.

Ein hoher Wert bedeutet, dass während des Tests mehr von deinem Code
ausgeführt wurde.
%
Siehe Plugin \module{pytest-cov} (Paket \filename{python3-pytest-cov});
\url{https://github.com/pytest-dev/pytest-cov}.
%
% Durch das Plugin \module{pytest-cov}\cite{pytest-cov} (Paket \filename{python3-pytest-cov})
% zeigt dir pytest an, welche Codezeilen durch einen Test noch nicht getestet
% wurden.

\item[Schreibe unabhängige Testeinheiten]

Tests dürfen nie von anderen Tests abhängen, die Reihenfolge
ist unwichtig und sie sind sowohl alleine als auch innerhalb
der Test-Suite problemlos ausführbar.

\item[Organisiere deine Tests wie dein Quellverzeichnis]

Um deine Tests besser auffindbar zu machen,
verwende möglichst die gleiche Struktur und Namen bei den Tests
wie im Quellcode. Beispielsweise gibt es zu \filename{src/mypackage/foo/bar.py}
den Test \filename{tests/foo/test\_bar.py}.
% Das pytest-Framework wird alle finden solange du sie
% nach der üblichen Namenskonvention benennst (siehe Abschnitt~\ref{sec.names}).

\item[Schreibe \gquote{schnell} durchlaufende Tests]

Je schneller desto besser. Ein langsamer Test bremst dich
beim Entwickeln aus.
% Eine Test-Suite die mehrere Stunden zum
% Testen brauchst, wirst du selten aufrufen. ;-)
%
Tests die langsamer sind, sollten markiert werden siehe
Abschnitt~\ref{sec.marker.usedefined}.

\item[Lerne dein Test-Framework]

Selbsterklärend, oder? ;-)
% Damit du weißt, wie du dein Test-Framework erweitern kannst, wie du
% einzelnen Test startest oder die Fehlerausgabe zu lesen hast.

\item[Verwende beschreibende Namen für deine Testfunktionen]

Längere Namen für deine Testfunktionen sind ok, da sie beim
Testen ausgegeben werden können und somit eine bessere Übersicht
geben.

% In deinem normalen Quellcode sind lange Namen eher verpönt, aber
% als Testfunktionen ist es völlig ok. Der Grund ist, dass diese Namen
% beim Testen ausgegeben werden können und somit eine bessere Übersicht
% geben.
\end{description}


\section{Vorgehensweise bei TDD}
% Was ist das besondere an Testen bzw.\ \emph{Test Driven Development}%
% (TDD)?
Wie geht man üblicherweise vor?
Die folgende Sequenz ist aus \cite{wikipedia.tdd} entnommen:

\begin{enumerate}
\item \textbf{\sffamily Schreibe einen Test}

Jedes neue Feature beginnt zuerst mit einem Test.
Überlege dir zuerst, was du testen möchtest. Was erwartet deine Funktion,
was gibt sie zurück? Schreibe dazu einen Test.
% Schreibe einen Test das den neuen Aspekt deines Programms überprüft.

\item \textbf{\sffamily Führe den Tests aus (muss fehlschlagen)}

Die zu testende Funktion ist anfangs leer und wird dadurch
fehlschlagen.

\item \textbf{\sffamily Schreibe den Code}

Schreibe gerade genug Code damit der Test durchläuft.
Der Code muss nicht perfekt sein, er kann sogar unelegant oder
ineffizient sein. Wichtig ist, dass der Test erfolgreich ist.

\item \textbf{\sffamily Ausführen aller Tests}

Wenn alle Tests erfolgreich waren, erfüllt der neue Code
alle Test-Kriterien. Weder verschlechtert der neue Code
andere Tests, noch bricht er sie.
Sollte das dennoch der Fall sein, muss der neue Code angepasst
werden bis alle Tests wieder erfolgreich sind.

\item \textbf{\sffamily Überarbeiten deines Codes (Refactoring)}

Der lauffähige Code wird nun \gquote{optimiert} hinsichtlich
der Lesbarkeit, Stil, Verhalten oder sogar der Laufzeit.

\item \textbf{\sffamily Wiederhole}
\end{enumerate}

Aus praktischen Gründen kann die Implementierung \emph{vor}
dem Test entstehen.
Solange zu jedem Feature/Funktion ein Test existiert, liegt
das im Ermessen des Entwicklers.

\section{Was ist pytest?}
\label{sec.pytest.definition}
\cmd{pytest}\cite{krekel.pytest} ist ein Framework, das es erlaubt,
Tests mit Hilfe von \pyth{assert} zu schreiben.
Das pytest-Framework erkennt Tests von \module{unittest} und
\module{doctests}.


\section{Installieren von \cmd{pytest}}
\begin{enumerate}
\item Führe folgenden Befehl aus:
\begin{Verbatim}[commandchars=\\\{\}]
$ sudo zypper install python3-pytest
\end{Verbatim}

\item Überprüfe, ob die richtige Version installiert ist:
\begin{Verbatim}[commandchars=\\\{\}]
$ pytest --version
This is pytest version a.b.c, imported from
/usr/lib/python3.6/site-packages/pytest.py
\end{Verbatim}
\end{enumerate}

Das pytest-Framework lässt sich mittels Plugins erweitern. Alle
Pakete sind nach der Konvention \filename{python3-pytest-PLUGINNAME}
benannt.


\section{Konfigurieren von pytest}
Durch Konfigurationsoptionen kannst du das Verhalten von pytest anpassen.
Je nach Vorliebe lassen sich die Optionen in folgenden INI-Dateien
eintragen (wird in dieser Reihenfolge von pytest gelesen):

\begin{itemize}
\item \filename{pytest.ini}
\item \filename{tox.ini}
\item \filename{setup.cfg}
\end{itemize}

Alle Optionen müssen unter dem Abschnitt \verb![pytest]! erscheinen. Außnahme
ist \filename{setup.cfg}: hier musst du \verb![tool:pytest]! schreiben.

Die folgenden Unterabschnitte geben nur einen kleinen Einblick. Weitere
Optionen findest du unter \cite{krekel.pytest.cmd.option}.
%\emph{Configuration Options} in der API Referenz
% unter \url{https://docs.pytest.org}.


\subsection{Bestimmen der minimalen pytest-Version}
% Hiermit kannst du die minimale Version von pytest festlegen.
Alles kleiner als die angegebene Version führt zu einer
Fehlermeldung:

\begin{inicode}
# pytest.ini oder tox.ini
[pytest]
minversion = 3.0  # will fail if we run with pytest-2.8
\end{inicode}


\subsection{Setzen von Standardoptionen}
\label{sec.config.addopts}
Mittels \option{addopts} kannst du Standardoptionen an pytest
weiterreichen. Beispielsweise zeigt die Option \option{-v} (verbosity)
die Test-ID (siehe Abschnitt~\ref{sec.pytest.test-id}) an:

\begin{inicode}
# pytest.ini oder tox.ini
[pytest]
addopts = -v
\end{inicode}

Wäre das gleiche, als wenn du \cmd{pytest -v} aufgerufen hättest.


\subsection{Setzen der Suchpfade zum Finden von Tests}
Standardmäßig durchsucht pytest alle Verzeichnisse ausgehend von deinem
Projekverzeichnis. Wenn auf der Kommandozeile keine spezifischen Verzeichnisse,
Dateien oder Test-IDs (Abschnitt~\ref{sec.pytest.test-id}) angegeben
wurde, kann folgende Einstellung helfen:

\begin{inicode}
# pytest.ini oder tox.ini
[pytest]
testpaths = tests/  docs/
\end{inicode}

Dadurch werden keine unerwünschten Tests erkannt die sich eventuell in einer
virtuellen Python-Umgebung befinden könnten.

\subsection{Ausschließen von Suchpfaden}
Schließt bestimmte Pfade beim rekursiven Suchen aus. Das kann nützlich sein,
wenn du deine virtuelle Python-Umgebung im Verzeichnis \filename{.venv}
nicht durchsuchen lassen möchtest.
Es dürfen ein oder mehrere Werte gesetzt werden:

\begin{inicode}
# pytest.ini oder tox.ini
[pytest]
norecursedirs = .git/ .venv/ tmp*/
\end{inicode}


\subsection{Ändern der Namenskonventionen}
\label{sec.names}
Standardmäßig sucht pytest nach folgenden Namen:

\begin{itemize}
\item Test-Dateien: \filename{test\_*.py} oder \filename{*\_test.py}
\item Test-Klassen: \cmd{Test*}
\item Test-Funktionen: \cmd{test\_*}
\end{itemize}

Falls du eine andere Namenskonvention für deinen Tests verwendet hast,
setze folgende Werte:

\begin{inicode}
# pytest.ini oder tox.ini
[pytest]
python_classes = Check
python_functions = *_check
python_files =
   check_*.py
   example_*.py
\end{inicode}

Es dürfen ein oder mehrere Werte gesetzt werden. Durch obige Konfiguration
erkennt pytest alle Dateien die mit \filename{check\_} oder \filename{example\_}
beginnen und mit \filename{.py} enden.

\iffalse
\section{Vergleich \cmd{unittest} vs. \cmd{pytest}}
Lege folgende Dateien an:
\begin{python}
# Inhalt der Datei project.py
def add(x, y):
    """Gibt zwei Zahlen oder Strings zurueck,
    die addiert/verkettet wurden
    """
    return x + y
\end{python}

\subsection{Beispiel mit \cmd{unittest}}
\cmd{unittest} ist ein Framework, ursprünglich von JUnit inspiriert.
\begin{python}
# Inhalt der Datei test_project_unittest.py
import unittest
from project import add

class TestAddMethod(unittest.TestCase):
    def test_numbers_1_2(self):
        """Testet, ob die Addition von zwei Ganzzahlen
        die korrekte Summe zurueckgibt
        """
        self.assertEqual(add(1, 2), 3)

    def test_strings_a_b(self):
        """Testet, ob die Addition von zwei Strings
        die beiden Strings als eine verkettete Zeichenfolge
        zurueckgibt
        """
        self.assertTrue(add("a", "b") == "ab")
        self.assertFalse(add("a", "b") == "abc")

if __name__ == '__main__':
    unittest.main()
\end{python}
\fi


\section{Wählen eines Test-Layouts}
\label{sec.layout}
Mărieș empfiehlt in \cite{maries.pypack} das Quellcodes-
(\filename{src/}) und Tests-Verzeichnis (\filename{tests/})
zu trennen.
Durch diese Trennung werden Tests nicht installiert.

% Es wird empfohlen\cite{maries.pypack} dein Quellverzeichnis wie folgt zu
% strukturieren:

\iffalse
\begin{verbatim}
project_root_dir/
    +-- setup.py
    +-- src/
    |   +-- mypkg/
    |       +-- __init__.py
    |       +--  app.py
    |       +--  view.py
    +-- tests/
        +-- __init__.py
        +-- conftest.py
        +-- foo/
        |    +-- __init__.py
        |    +-- test_view.py
        +-- bar/
            +-- __init__.py
            +-- test_view.py
\end{verbatim}

Dadurch sind Quellcodes (\filename{src/}) und Tests (\filename{tests/})
sauber getrennt. Durch diese Trennung werden Tests nicht installiert.
\fi


\section{Beispiel}
Um das folgende Beispiel einfach zu halten, wurde auf das
empfohlene Test-Layout verzichtet. Wir verwenden zwei Dateien
in einem Verzeichnis \filename{pytest-x}:

\begin{itemize}
\item \filename{project.py}: enthält den Quellcode
\item \filename{test\_project.py}: enthält alle Tests für
\filename{project.py}.
\end{itemize}

\subsection{Das zu testende Projekt}
Unsere \filename{project.py}-Datei enthält:

\begin{python}
# Content of project.py
def add(x, y):
    """Expects two digits or strings; adds or concats
    both and return the result
    """
    return x + y
\end{python}

\subsection{Die Testdatei \filename{test\_project.py}}
Eine Testdatei für pytest wird üblicherweise das zu testende
Projekt importieren (hier: \filename{project.py}).

Innerhalb einer Testfunktion wird mittels \pyth{assert} der
Vergleich vorgenommen zwischen realem Ergebnis und dem zu
erwartendem:

\begin{python}
# Content of test_project.py
import project

def test_add_numbers_1_2():
    assert project.add(1, 2) == 3

def test_add_strings_a_b():
    assert project.add("a", "b") == "ab"
\end{python}

\section{Ausführen von Tests}
Das pytest-Framework erlaubt dir alle Tests auszuführen oder
ganz bestimmte (anhand der Datei, einer Markierung oder einer
Test-ID).

\subsection{Anzeigen aller gefundenen Testfunktionen}
\label{sec.pytest.collect-only}
Praktisch ist die Option \option{--collect-only}. Sie zeigt dir alle
gefundenen Tests an:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest --collect-only
[...]
collected 2 items
<Module 'test_project.py'>
  <Function 'test_add_numbers_1_2'>
  <Function 'test_add_strings_a_b'>
\end{Verbatim}

Hier siehst du das Modul \filename{test\_project\_pytest.py} das zwei
Testfunktionen enthält.


\subsection{Ausführen von allen Tests}
\label{sec.pytest.test-all}
Ohne weiteren Argumente sucht pytest alle Tests und führt sie aus:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest
[...]

test_project.py \textcolor{green}{..}                                   [100%]

\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}

Solltest du eine etwas detailierte Sicht bevorzugen, verwende die
Option \option{-v} um die ausgeführten Testfunktionen anzuzeigen
(siehe auch Abschnitt~\ref{sec.config.addopts}):

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v
[...]

test_project.py::test_add_numbers_1_2 \textcolor{green}{PASSED}         [ 50%]
test_project.py::test_add_strings_a_b \textcolor{green}{PASSED}         [100%]

\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}


\subsection{Einschränken auf Testdateien}
\label{sec.pytest.test-files}
Möchtest du gezielt nur eine oder mehrere Testdateien ausführen,
schreibst du sie als Argumente hinzu:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest test_project.py
[...]

test_project.py \textcolor{green}{..}                                   [100%]
\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}


\subsection{Einschränken auf Testfunktionen (Test-ID)}
\label{sec.pytest.test-id}
Falls du an einer bestimmten Testfunktion arbeitest, möchtest du evlt.\ nicht
deine komplette Test-Suite ausführen. Du kannst die Ausführung auf genau diese
Testfunktion einschränken. Dazu kennt pytest folgende Syntax
(sog.\ \emph{Test-ID}):

\begin{Verbatim}
<TEST_DATEI>::<TEST_NAME>
\end{Verbatim}

Es dürfen eine oder mehrere Einträge angegeben werden:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_numbers_1_2
[...]

test_project.py::test_add_numbers_1_2 \textcolor{green}{PASSED}         [100%]
\textcolor{green}{================= 1 passed in 0.02 seconds ================}
\end{Verbatim}

Falls die Test-Funktion parametrisiert ist, erweitert sich die Syntax um
die Parameter:

\begin{Verbatim}
<TEST_DATEI>::<TEST_NAME>[Parameter1-Parameter2-...]
\end{Verbatim}

Die Test-Funktion aus Abschnitt~\ref{sec.pytest.mark.parametrize} wird wie
folgt ausgegeben:

\begin{Verbatim}[commandchars=\\\{\}]
[...]
test_project.py::test_eval[3+5-8] \textcolor{green}{PASSED}             [___%]
test_project.py::test_eval[2+4-6] \textcolor{green}{PASSED}             [___%]
\end{Verbatim}

Soll bei einem nächsten Durchlauf nur der erste ausgeführt werden, schreibst
du in der Shell:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_eval[3+5-8]
\end{Verbatim}



\section{Lesen von fehlgeschlagenen Tests}
In den vorigen Abschnitte waren alle Tests erfolgreich. In diesem
Abschnitt wollen wir einen Test nachstellen der fehlschlägt.

Wir wollen hierzu eine neue Funktion \func{sub()} zu unseren Dateien hinzufügen,
welche zwei Zahlen subtrahiert:

\begin{enumerate}
\item In \filename{test\_project.py} schreiben wir zuerst den Test:

\begin{python}
def test_sub_numbers_3_1():
    assert project.sub(3, 1) == 2
\end{python}

\item In \filename{project.py} implementieren wir die (fehlerhafte) Funktion
  \func{sub}:

\begin{python}
def sub(x, y):
    return x + y  # should be minus!
\end{python}
\end{enumerate}

Beim Ausführen erhälst du:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest
[...]

test_project.py \textcolor{green}{..}\textcolor{red}{F}                                  [100%]

========================= Failure =========================
\textcolor{red}{___________________ test_sub_numbers_3_1 __________________}
    def test_sub_numbers_3_1():
\textcolor{red}{>       assert project.sub(3, 1) == 2}
\textcolor{red}{E       assert 4 == 2}
\textcolor{red}{E        +  where 4 = <function sub at 0x...>(3, 1)}
\textcolor{red}{E        +    where <function sub at 0x...> = project.sub}

\textcolor{red}{test_project.py}:11: AssertionError
\textcolor{red}{============ 1 failed, 2 passed in 0.15 seconds ===========}
\end{Verbatim}

pytest zeigt dir genau an, \emph{wo} etwas fehlgeschlagen ist (in der
Test-Funktion \func{test\_sub\_numbers\_3\_1}) und \emph{was} verglichen
wurde (\pyth{assert 4 == 2}).
So kannst du dich auf die Fehlersuche machen und siehst, dass der Operator
in der Funktion \func{sub()} falsch ist (sollte Minus statt Plus sein).


\section{Überprüfen von erwarteten Ausnahmen}
Eine gute Test-Suite testet das erwartete Verhalten sowohl, wenn die Eingabe
korrekt ist, als auch, wenn die Eingabe eine Ausnahme auslöst.

Unser Projekt soll in der Funktion \func{add(x, y)} den Typ überprüfen. Dabei soll
es ungleiche Datentypen in den Argumenten mit einem \pyth{TypeError} zurückweisen.
Dies erfordert ein \emph{Refactoring} des Projekts:

\begin{enumerate}
\item Zuerst wird der Test in der Datei \filename{test\_project.py} integriert:

\begin{python}
def test_instance_a_2():
    with pytest.raises(TypeError):
        add("a", 2)
\end{python}

\item Danach erfolgt die Implementierung in der Datei \filename{project.py}:

\begin{python}
def add(x, y):
    if isinstance(x, type(y)) or isinstance(y, type(x)):
        raise TypeError("Incompatible value")
    return x + y
\end{python}


\item Ergebnis beim Ausführen:
\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_both_a_2
[...]
test_project.py::test_instance_a_2 \textcolor{green}{PASSED}         [100%]
\end{Verbatim}
\end{enumerate}


\section{Markieren von Test-Funktionen}
Es gibt zwei Typen von Markern: \emph{eingebaute} und \emph{benutzerdefinierte}.
Für beide verwendest du den \func{pytest.mark} Decorator.

Durch Marker kannst du Test-Funktionen nach bestimmten Eigenschaften
gruppieren (kompatibles Betriebssystem, erlaubte Python-Version,
Ausführungszeit usw.) und sie
bei der Ausführung selektieren (oder deselektieren).

Du kannst alle verfügbaren Marker anzeigen lassen mit dem Befehl
\cmd{pytest -{}-marker}.

\subsection{Verwenden von eingebauten Markern}
Eingebaute Marker sind \func{skip}, \func{skipif} und \func{xfail}.
Der Marker \func{parametrize} ist ein Sonderfall und wird in
Abschnitt~\ref{sec.pytest.mark.parametrize} beschrieben.

\begin{description}
\item[Überspringen von Test-Funktionen]
Um eine Test-Funktion nicht auskommentieren zu müssen, markiere sie mittels
\func{skip}:

\begin{python}
@pytest.mark.skip(reason="needs more time")
def test_the_difficult_function():
\end{python}

\item[Bedingtes Überspringen]
Der Marker \func{skipif} kann nützlich sein, wenn das Überspringen an eine
Bedingung geknüpft ist. Folgende Funktion wird nur ausgeführt, wenn die
Python-Version 3.6 oder höher ist:

\begin{python}
import sys
@pytest.mark.skipif(sys.version_info < (3, 6),
                    reason="requires python3.6 or higher")
def test_function():
\end{python}

Wird ein Marker öfters benötigt, ist es sinnvoll diesen einmal zu definieren
und dann die Variable zu verwenden:

\begin{python}
import pytest

py36_marker = pytest.mark.skipif(sys.version_info < (3, 6),
                  reason="requires python3.6 or higher")

@py36_marker
def test_function():
\end{python}

\item[Erwarteter Fehler]
Der Marker \func{xfail} nutzt du, um Test-Funktionen zu markieren, die
fehlschlagen:

\begin{python}
@pytest.mark.xfail
def test_function_to_fail():
\end{python}

Das kann nützlich sein, wenn die Implementierung noch nicht steht, du aber
weißt, dass die Test-Funktion nicht erfolgreich sein kann. Gewissermaßen
wie eine TODO-Liste.
\end{description}

\subsection{Nutzen von benutzerdefinierten Markern}
\label{sec.marker.usedefined}
Wenn die eingebauten Marker nicht ausreichen, kannst du selber welche
definieren. So kannst du z.~B.\ Test-Funktionen gruppieren nach Webaspekten
(Browser vs.\ CLI), Laufzeit (schnell vs.\ langsam) usw.

Die folgende Test-Funktion wird als \gquote{\func{slow}} markiert um
anzuzeigen, dass diese Funktion länger in der Ausführung benötigt:

\begin{python}
import pytest

@pytest.mark.slow
def test_search_for_prime_numbers():
\end{python}

Durch die Markierung kannst du bei der Ausführung wählen:

\begin{itemize}
\item Nur Tests auswählen die mit \func{slow} markiert sind:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v -m slow
\end{Verbatim}

\item Alle anderen Tests auswählen, die \emph{nicht} \func{slow} sind:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v -m "not slow"
\end{Verbatim}
\end{itemize}


\section{Parametrisieren von Test-Funktionen}
\label{sec.pytest.mark.parametrize}
Typischerweise enthalten Test-Funktionen Daten die \gquote{hardcodiert}
im Funktionskörper verwendet werden.
Soll eine Test-Funktion jedoch unterschiedliche Daten annehmen können,
müsste für jede Kombination unterschiedliche Funktionen geschrieben werden.

Um diesen Aufwand zu minimieren kannst du den Decorator
\func{pytest.mark.parametrize} verwenden. Er erwartet:

\begin{itemize}
\item 1. Argument enthält eine Zeichenfolge.

Inhalt ist eine kommaseparierte
 Liste von Argumenten. Die Anzahl richtet sich nach den zwingend
 anzugebenen Argumenten der zu testenden Funktion. Zusätzlich wird
 (typischerweise) ein \verb!expected! angegeben.
\item 2. Argument enthält eine Liste mit Tupeln.

Sie enthalten die Daten und den Erwartungswert.
\end{itemize}

Soll beispielsweise die Python-Funktion \pyth{eval(source)} getestet werden,
erwartet sie ein zwingendes Argument nämlich den Python-Code.

Unsere Testdaten sind ein Tuple und enthalten jeweils ein Python-Code
und das Ergebnis davon. Daraus ergibt sich:

\begin{python}
# Inhalt von test_project.py
import pytest

@pytest.mark.parametrize("source, expected", [
  # First test:
  ("3+5", 8),
  # Second test:
  ("2+4", 6),
])
def test_eval(source, expected):
   assert eval(source) == expected
\end{python}

Ruft pytest diese Test-Funktion auf, wird der erste Test wie folgt
ablaufen:

\begin{enumerate}
\item Das erste Tuple \pyth{("3+5", 8)} wird aus den Testdaten entnommen.
\item Das Tuple wird entpackt; \field{source} enthält \verb!"3+5"! und
  \field{expected} den Wert \verb!8!.
\item Die erhaltenen Werte werden der Funktion \func{test\_eval} übergeben.
\item Der Variablen werden in die Funktion eingesetzt und ausgeführt;
  die \pyth{assert}-Anweisung wird zu: \pyth{assert eval("3+5") == 8}.
\item Pytest entnimmt ein weiteres Tuple aus den Testdaten und wiederholt
   den Vorgang.
\end{enumerate}


\iffalse
Test-Funktionen benötigen Eingabewerte. Jedoch können Werte, z.~B.\ bei
Ganzzahlen Null, negative oder positive Werte annehmen. Diese alle zu
testen würde für jede Kombination eine eigene Test-Funktion benötigen.

Um diesen Aufwand zu minimieren, kannst du den Decorator
\func{pytest.mark.parametrize} verwenden:

\begin{python}
# Inhalt von test_project.py
import pytest

@pytest.mark.parametrize("source, expected", [
  # First test:
  ("3+5", 8),
  # Second test:
  ("2+4", 6),
])
def test_eval(source, expected):
   assert eval(source) == expected
\end{python}

\begin{itemize}
\item Das erste Argument enthält eine Zeichenkette. Sie enthält
komma-separierte Argumentnamen.
Üblicherweise enthält es die Anzahl der Argumente welche deine zu
testende Funktion erwartet. Zusätzlich wird ein Argument angegeben,
welches das erwartete Ergebnis enthält (typischerweise als
\verb!expected! benannt).

\item Das zweite Argument ist eine Liste mit Tupeln. Jedes dieser Tuple
enthält einen Testfall. Der Inhalt eines Tuples beschreibt die Daten welche die
zu testende Funktion übergeben bekommt und das erwartete Ergebnis.
\end{itemize}

Diese Tupel werden der Reihe nach in die Test-Funktion eingesetzt. Dadurch
ist es sehr einfach weitere Testdaten hinzuzufügen ohne die eigentliche
Test-Funktion zu ändern.

Beim Ausführen erscheint:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_eval
...
collected 2 items

test_project.py::test_eval[3+5-8] \textcolor{green}{PASSED}              [ 50%]
test_project.py::test_eval[2+4-6] \textcolor{green}{PASSED}              [100%]
\end{Verbatim}
\fi

\section{Einsetzen von Fixtures}
Fixtures sind Funktionen die mit dem Decorator
\func{pytest.fixture} markiert wurden. Somit kann pytest erkennen, dass es sich
um eine Fixture handelt.

Um eine Fixture zu nutzen, schreibst du den Fixture-Namen
als Argument in deine Test-Funktion. Dadurch wird sie automatisch von pytest
aufgerufen und hinterlässt einen Rückgabewert der durch die Fixture definiert ist.

Fixture gibt es in zwei Typen: \emph{eingebaut} und
\emph{benutzerdefiniert}.

Du kannst alle verfügbaren Fixtures anzeigen lassen mit dem Befehl
\cmd{pytest -{}-fixtures}.

\subsection{Verwenden von eingebauten Fixtures}
\label{sec.fixture.builtin}
Eingebaute Fixtures sind z.~B.\ \func{cache}, \func{capsys}, \func{pytestconfig},
\func{tmpdir} und andere. In diesem Abschnitt gehen wir auf die Fixture
\func{tmpdir} genauer ein. Details findest du in \cite{krekel.pytest.api.fixtures}.

Die Fixture \func{tmpdir} erzeugt ein temporäres Verzeichnis das einzigartig
für jede Test-Funktion ist und bei jedem Durchlauf anders benannt ist.
Als Rückgabewert erhälst du ein \module{py.path.local}\cite{py.path} Objekt.

Im folgenden Beispiel nutzt die Test-Funktion die Fixture \func{tmpdir}
und erstellt eine Datei in dem temporären Verzeichnis:

\begin{python}
def test_create_file(tmpdir: py.path.local):
    # Create path to hello.txt:
    hello = tmpdir.join("hello.txt")
    # write text to hello.txt:
    hello.write_text("Hello\nWorld", encoding="utf-8")
    # Now call your test function with the string of the path:
    assert count_linebreaks(hello.strpath) == 1
\end{python}

Durch die Fixture \func{tmpdir} entfällt das manuelle Erzeugen eines temporären
Verzeichnisses.

\subsection{Verwenden von benutzerdefinierten Fixtures}
Benutzerdefinierte Fixtures kannst du in einer Test-Datei schreiben oder in
die Datei \filename{conftest.py}. Letzere hat den Vorteil, dass pytest diese
Datei automatisch lädt und somit ein Import unnötig wird.

Weitere spannende Möglichkeiten zeigt dir \cite{krekel.pytest.fixtures}.
Alle vorgestellten Beispiele benötigen ein \pyth{import pytest} welches im
Code nicht explizit angegeben wurde.

\iffalse
\subsubsection{Zurückgeben von Daten}
Im folgenden Beispiel wird eine Fixture geschrieben, welche eine Liste (=Daten)
zurückgibt. Es sind die ersten fünf Werte der Fibonacci-Folge:

\begin{python}
@pytest.fixture
def fibonacci_5():
    return [0, 1, 1, 2, 3, 5]
\end{python}
\fi


\subsubsection{Nutzen von eingebauten Fixtures}
Fixtures können auch andere Fixtures nutzen. Somit kannst du beispielsweise
eine Datei in einem temporären Verzeichnis anlegen und es mit bestimmten
Daten füttern.

\begin{python}
@pytest.fixture
def datafile(tmpdir):
   hello = tmpdir.join("hello.txt")
   hello.write_text("Hello\nWorld", encoding="utf-8")
   return hello
\end{python}

Durch diese Fixture vereinfacht sich der Code der Testfunktion
\func{test\_create\_file} aus Abschnitt~\ref{sec.fixture.builtin} wie folgt:

\begin{python}
def test_create_file(datafile: py.path.local):
    assert count_linebreaks(datafile.strpath) == 1
\end{python}


\subsubsection{Parametrisieren von Fixtures}
Falls deine Fixture verschiedene Werte verwenden soll, kannst du eine Fixture
Parametrisieren.
Die folgende Fixture liest eine Konfigurationsdatei entwender vom
Benutzerverzeichnis oder aus \filename{/etc} und die Testfunktion wird
zweimal aufgerufen:

\begin{python}
import pytest

@pytest.fixture(params=["~/.config/foo", "/etc/fooconfig"])
def config(request):
    configfile = open(request.param, 'r')
    yield configfile
    config.close()

def test_fooconfig(config):
    # test if you can read a line:
    assert config.readline()
\end{python}

\subsection{Definieren von Scopes für Fixtures}
Der \func{pytest.fixture}-Decorator kann auch ein Argument
\field{scope} aufnehmen. Mögliche Werte von \field{scope}
sind: \field{function} (Standard), \field{class},
\field{module}, \field{package} oder \field{session}.

Durch einen \emph{Scope} (Gültigkeitsbereich) kann pytest
eine Fixture für jede Funktion erneut aufrufen oder es
auf Klassen, Module, Pakete oder die gesammte Session
beschränken.

Nehmen wir an du schreibst Tests um SSH zu testen und
dafür hast du eine SSH-Fixture geschrieben.

Das Erstellen der SSH-Dienstes ist jedoch zeitaufwändig. Um
nicht bei jedem Test, der den Dienst benötigt, Zeit zu
verschwenden, wird die SSH-Fixture auf die Session beschränkt.
Dadurch erhält jede Test-Funktion dasselbe SSH-Objekt:

\begin{python}
# in conftest.py
import pytest

@pytest.mark.fixture(scope="session")
def ssh_service():
    #  creates an SSH service object
    yield service_object
    # destorys the SSH service object

# in a test file:
def test_git_amount(ssh_service):
    # test the SSH service
\end{python}

Beim Starten von pytest wird der SSH-Dienst gestartet und
wenn die letzte Test-Funktion ausgeführt wurde, wird der
SSH-Dienst gestoppt.

% --------------------------------------------------------------------
\begin{thebibliography}{10}
\bibitem{beck.tdd}
\bauthor{Kent Beck},
\btitle{Test-Driven Development by Example},
Addison Wesley, ISBN 978-0-321-14653-3.

\bibitem{py.path}
\bauthor{Holger Krekel} et.~al.
\btitle{py.path},
\url{https://py.readthedocs.io/en/latest/path.html}

\bibitem{krekel.pytest.cmd.option}
\bauthor{Holger Krekel} et.~al.,
\btitle{Configuration Options},
\url{https://docs.pytest.org/en/latest/reference.html#configuration-options}

\bibitem{krekel.pytest.api.fixtures}
\bauthor{Holger Krekel} et.~al.,
\btitle{API: Fixtures},
\url{https://pytest.org/en/latest/reference.html#fixtures}

\bibitem{krekel.pytest}
\bauthor{Holger Krekel} et.~al.,
\btitle{pytest: helps you write better programs},
\url{https://pytest.org}

\bibitem{krekel.pytest.fixtures}
\bauthor{Holger Krekel} et.~al.,
\btitle{pytest fixtures: explicit, modular, scalable},
\url{https://pytest.org/en/latest/fixture.html}

\bibitem{maries.pypack}
\bauthor{Ionel Cristian Mărieș},
\btitle{Packaging a Python Library},
\url{https://blog.ionelmc.ro/2014/05/25/python-packaging/}

\iffalse
\bibitem{pytest-cov}
\bauthor{Marc Schlaich} et.~al.,
\btitle{Coverage Reports for pytest},
\url{https://github.com/pytest-dev/pytest-cov}
\fi

\bibitem{usersnap.com}
\bauthor{John Sonmez}
\btitle{7 Common Types of Software Testing}
\url{https://usersnap.com/blog/software-testing-basics/}

\bibitem{wikipedia.tdd}
\bauthor{Wikipedia},
\btitle{Test-driven development}
\url{https://en.wikipedia.org/wiki/Test-driven_development}

\end{thebibliography}

\end{multicols}
\end{document}
